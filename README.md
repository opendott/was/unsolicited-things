# Unsolicited Things - a.k.a. The Buzzy Things

A wearable design probe to explore reactions to vague/opaque messages from the "internet".

An alien device is giving unsolicited messages in form of vibrations and light.

Who sent this message?

What sent this message?

What does the message mean?

Do I need to worry?

Is everything fine?

How can I respond?

---

This code is part of the explorations around wearable IoT and the self within the [OpenDoTT](https://opendott.org) project.
Originally planned to be used as a cultural probe in [The Lost Manual](https://opendott.org/the-lost-manual) study.