/*
 * Based on the Watchdog Sleep Example 
 * Demonstrate the Watchdog and Sleep Functions
 * LED on digital pin 1
 * 
 * KHM 2008 / Lab3/  Martin Nawrath nawrath@khm.de
 * Kunsthochschule fuer Medien Koeln
 * Academy of Media Arts Cologne
 *
 * Modified on 5 Feb 2011 by InsideGadgets (www.insidegadgets.com)
 * to suit the ATtiny85 and removed the cbi( MCUCR,SE ) section 
 * in setup() to match the Atmel datasheet recommendations
 * 
 * Mofified on 11 Feb 2020 by Jens Alexander Ewald
 * to adapt for speculative wearable design probes
 * "the unsolicited" - a random notifier for a speculative IoT
 */

#include <avr/sleep.h>
#include <avr/wdt.h>

#ifndef cbi
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif
#ifndef sbi
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif

                // D0 maps to pin 5 on DIL
int pinLed = 1; // D1 maps to pin 6 on DIL
int motor  = 2; // D2 maps to pin 7 on DIL

volatile int f_wdt = -1; // Set to 0 to have an initial run after boot

void pattern()
{
  size_t count = random(4, 10);
  for (size_t i = 0; i < count; i++)
  {
    digitalWrite(pinLed, HIGH); // let led blink
    digitalWrite(motor, HIGH);  // let led blink
    delay(random(30, 100));
    digitalWrite(pinLed,LOW);
    digitalWrite(motor,LOW);
    delay(random(100, 500));
  }
}

void setup()
{
  pinMode(pinLed, OUTPUT);
  pinMode(motor, OUTPUT);

  // 0=16ms, 1=32ms,2=64ms,3=128ms,4=250ms,5=500ms
  // 6=1 sec,7=2 sec, 8=4 sec, 9= 8sec
  setup_watchdog(9); // approximately 8 seconds sleep
}

// Calculate the amount of watchdog sleep cycles
static unsigned int resetCounter() {
  // 1h = 3600sec
  // 1m = 60sec
  return random(60, 300) >> 3; // 1h // divide by 8 is equal to shift it by 3 to the right
}

int counter = resetCounter(); // this is the multiplier for the watchdog sleep time

void loop()
{
  // This is for testing purpose only
  if (f_wdt == -1) {
    pattern();
    f_wdt = 1;
  }
  if (f_wdt == 1)
  {            // wait for timed out watchdog / flag is set when a watchdog timeout occurs
    f_wdt = 0; // reset flag
    if (counter > 0) {
      counter--;
    } else {
      pattern();
      counter = resetCounter(); // reset the counter for a new countdown
    }

    pinMode(pinLed, INPUT); // set all used port to intput to save power
    pinMode(motor, INPUT);
    system_sleep();
    pinMode(pinLed, OUTPUT); // set all ports into state before sleep
    pinMode(motor, OUTPUT);
  }
}

// set system into the sleep state
// system wakes up when wtchdog is timed out
void system_sleep()
{
  cbi(ADCSRA, ADEN); // switch Analog to Digitalconverter OFF

  set_sleep_mode(SLEEP_MODE_PWR_DOWN); // sleep mode is set here
  sleep_enable();

  sleep_mode(); // System sleeps here

  sleep_disable();   // System continues execution here when watchdog timed out
  sbi(ADCSRA, ADEN); // switch Analog to Digitalconverter ON
}

// 0=16ms, 1=32ms,2=64ms,3=128ms,4=250ms,5=500ms
// 6=1 sec,7=2 sec, 8=4 sec, 9= 8sec
void setup_watchdog(int ii)
{

  byte bb;
  int ww;
  if (ii > 9)
    ii = 9;
  bb = ii & 7;
  if (ii > 7)
    bb |= (1 << 5);
  bb |= (1 << WDCE);
  ww = bb;

  MCUSR &= ~(1 << WDRF);
  // start timed sequence
  WDTCR |= (1 << WDCE) | (1 << WDE);
  // set new watchdog timeout value
  WDTCR = bb;
  WDTCR |= _BV(WDIE);
}

// Watchdog Interrupt Service / is executed when watchdog timed out
ISR(WDT_vect)
{
  f_wdt = 1; // set global flag
}
